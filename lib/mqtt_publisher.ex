defmodule MqttTest.Publisher do
  use GenServer

  def start_link(params \\ {}) do
    GenServer.start_link(__MODULE__, params)
  end

  @impl GenServer
  def init(_c) do
    with {:ok, pid} <- connect() do
      {:ok, pid}
    end
  end

  defp connect() do
    {:ok, _pid} =
      Tortoise.Connection.start_link(
        client_id: "publisher",
        user_name: System.get_env("MQTT_USER"),
        password: System.get_env("MQTT_PWD"),
        server: {
          Tortoise.Transport.SSL,
          cert: "/etc/vernemq/certs/server.crt",
          keyfile: "/etc/vernemq/certs/server.key",
          host: System.get_env("MQTT_IP"),
          port: 8883
        },
        handler: {Tortoise.Handler.Default, []}
      )
  end

  def publish(client_id, topic, payload) do
    Tortoise.publish(client_id, topic, payload, qos: 0)
  end

  @impl true
  def handle_cast({:publish, payload}, state) do
    publish("publisher", "temp", payload)
    {:noreply, state}
  end
end
