defmodule MqttTest.Subscriber do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, {})
  end

  @impl true
  def init(_params) do
    subscribe()
  end

  defp subscribe(client_id \\ "my_subscriber", topic \\ {"temp", 0}) do
    Tortoise.Supervisor.start_child(
      client_id: client_id,
      user_name: System.get_env("MQTT_USER"),
      password: System.get_env("MQTT_PWD"),
      server: {
        Tortoise.Transport.SSL,
        cacertfile: :certifi.cacertfile(),
        certfile: "/etc/vernemq/certs/server.crt",
        keyfile: "/etc/vernemq/certs/server.key",
        host: System.get_env("MQTT_IP"),
        port: 8883,
        verify: :verify_none
      },
      handler: {Tortoise.Handler.Logger, []},
      subscriptions: [topic]
    )
  end
end
